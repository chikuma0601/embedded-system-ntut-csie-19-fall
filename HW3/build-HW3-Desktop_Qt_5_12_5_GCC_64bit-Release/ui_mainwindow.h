/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QLabel *targetLabel;
    QPushButton *openTargetButton;
    QPushButton *openTemplateButton;
    QPushButton *closeButton;
    QLabel *templateLabel;
    QPushButton *findContoursButton;
    QPushButton *circleButton;
    QPushButton *triangleButton;
    QPushButton *squareButton;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1089, 700);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        targetLabel = new QLabel(centralwidget);
        targetLabel->setObjectName(QString::fromUtf8("targetLabel"));
        targetLabel->setGeometry(QRect(10, 10, 400, 300));
        targetLabel->setMinimumSize(QSize(400, 300));
        targetLabel->setMaximumSize(QSize(400, 300));
        openTargetButton = new QPushButton(centralwidget);
        openTargetButton->setObjectName(QString::fromUtf8("openTargetButton"));
        openTargetButton->setGeometry(QRect(40, 350, 161, 61));
        openTemplateButton = new QPushButton(centralwidget);
        openTemplateButton->setObjectName(QString::fromUtf8("openTemplateButton"));
        openTemplateButton->setGeometry(QRect(40, 420, 161, 61));
        closeButton = new QPushButton(centralwidget);
        closeButton->setObjectName(QString::fromUtf8("closeButton"));
        closeButton->setGeometry(QRect(40, 490, 161, 51));
        templateLabel = new QLabel(centralwidget);
        templateLabel->setObjectName(QString::fromUtf8("templateLabel"));
        templateLabel->setGeometry(QRect(620, 20, 400, 300));
        templateLabel->setMinimumSize(QSize(400, 300));
        templateLabel->setMaximumSize(QSize(400, 300));
        findContoursButton = new QPushButton(centralwidget);
        findContoursButton->setObjectName(QString::fromUtf8("findContoursButton"));
        findContoursButton->setGeometry(QRect(290, 350, 161, 61));
        circleButton = new QPushButton(centralwidget);
        circleButton->setObjectName(QString::fromUtf8("circleButton"));
        circleButton->setGeometry(QRect(290, 430, 161, 51));
        triangleButton = new QPushButton(centralwidget);
        triangleButton->setObjectName(QString::fromUtf8("triangleButton"));
        triangleButton->setGeometry(QRect(290, 490, 161, 51));
        squareButton = new QPushButton(centralwidget);
        squareButton->setObjectName(QString::fromUtf8("squareButton"));
        squareButton->setGeometry(QRect(290, 560, 161, 51));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1089, 22));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        targetLabel->setText(QApplication::translate("MainWindow", "Target Image/Result", nullptr));
        openTargetButton->setText(QApplication::translate("MainWindow", "Open Target Image", nullptr));
        openTemplateButton->setText(QApplication::translate("MainWindow", "Open Template Image", nullptr));
        closeButton->setText(QApplication::translate("MainWindow", "Close App", nullptr));
        templateLabel->setText(QApplication::translate("MainWindow", "Template Image", nullptr));
        findContoursButton->setText(QApplication::translate("MainWindow", "Find Contours", nullptr));
        circleButton->setText(QApplication::translate("MainWindow", "Circle", nullptr));
        triangleButton->setText(QApplication::translate("MainWindow", "Triangle", nullptr));
        squareButton->setText(QApplication::translate("MainWindow", "Square", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
