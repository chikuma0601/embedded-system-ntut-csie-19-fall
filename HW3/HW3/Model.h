#ifndef MODEL_H
#define MODEL_H

#include <QPixmap>
#include <QImage>
#include "opencv2/core.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/xfeatures2d.hpp"

using namespace cv::xfeatures2d;
using namespace cv;

class Model
{
public:
    enum
    {
        CV_RETR_EXTERNAL  =0,
        CV_RETR_LIST      =1,
        CV_RETR_CCOMP     =2,
        CV_RETR_TREE      =3,
        CV_RETR_FLOODFILL =4,
    };

    enum
    {
        CV_CHAIN_CODE             =0,
        CV_CHAIN_APPROX_NONE      =1,
        CV_CHAIN_APPROX_SIMPLE    =2,
        CV_CHAIN_APPROX_TC89_L1   =3,
        CV_CHAIN_APPROX_TC89_KCOS =4,
        CV_LINK_RUNS              =5,
    };

public:
    void openTargetImage(QPixmap &);

    void openTemplateImage(QPixmap &);

    cv::Mat QPixmapConvertToMat(QPixmap &pimage);

    QPixmap MatConvertToQPixmap(cv::Mat &mat);

    QPixmap SelectContours(float ratio_thr1, float ratio_thr2);

    QPixmap FindContours();

    QPixmap FindCircle();

    QPixmap FindTriangle();

    QPixmap FindSquare();

    //void getFeatureVectors(cv::Mat , cv::Mat);

    QPixmap surf();

private:
    cv::Mat targetImage;
    cv::Mat templateImage;
};

#endif // MODEL_H
