#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QString>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_openTargetButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Image"), "/home/", tr("Image Files (*.png *.jpg *.bmp)"));
    targetImage.load(fileName);
    targetImage = targetImage.scaled(ui->targetLabel->size(), Qt::KeepAspectRatio);
    model.openTargetImage(targetImage);
    ui->targetLabel->setPixmap(targetImage);
}


void MainWindow::on_openTemplateButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Image"), "/home/", tr("Image Files (*.png *.jpg *.bmp)"));
    templateImage.load(fileName);
    templateImage = templateImage.scaled(ui->templateLabel->size(), Qt::KeepAspectRatio);
    model.openTemplateImage(templateImage);
    ui->templateLabel->setPixmap(templateImage);
}

void MainWindow::on_findContoursButton_clicked()
{
    targetImage = model.FindContours();
    targetImage = targetImage.scaled(ui->targetLabel->size(), Qt::KeepAspectRatio);
    ui->targetLabel->setPixmap(targetImage);
}

void MainWindow::on_circleButton_clicked()
{
    targetImage = model.FindCircle();
    targetImage = targetImage.scaled(ui->targetLabel->size(), Qt::KeepAspectRatio);
    ui->targetLabel->setPixmap(targetImage);
}

void MainWindow::on_triangleButton_clicked()
{
    targetImage = model.FindTriangle();
    targetImage = targetImage.scaled(ui->targetLabel->size(), Qt::KeepAspectRatio);
    ui->targetLabel->setPixmap(targetImage);
}

void MainWindow::on_squareButton_clicked()
{
    targetImage = model.FindSquare();
    targetImage = targetImage.scaled(ui->targetLabel->size(), Qt::KeepAspectRatio);
    ui->targetLabel->setPixmap(targetImage);
}

void MainWindow::on_closeButton_clicked()
{
    this->close();
}


void MainWindow::on_surfButton_clicked()
{
    QPixmap templateImage = model.surf();
    ui->targetLabel->setPixmap(templateImage);
}
