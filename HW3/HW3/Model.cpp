#include "Model.h"
#include <vector>
#include <iostream>

void Model::openTargetImage (QPixmap & image)
{
    targetImage = QPixmapConvertToMat(image);
}

void Model::openTemplateImage(QPixmap & image)
{
    templateImage = QPixmapConvertToMat(image);
}

cv::Mat Model::QPixmapConvertToMat(QPixmap &qimage)
{
    QImage image = qimage.toImage();
    cv::Mat mat(image.height(), image.width(), CV_8UC4,
                 const_cast<uchar*>(image.bits()),
                 static_cast<size_t>(image.bytesPerLine()));
    cv::cvtColor(mat, mat, cv::COLOR_BGR2RGB);
    return mat;
}

QPixmap Model::MatConvertToQPixmap(cv::Mat &mat)
{
    QImage image(mat.data, mat.cols, mat.rows, static_cast<int>(mat.step), QImage::Format_RGB888);
    QPixmap qimage;
    qimage.convertFromImage(image);
    return qimage;
}

QPixmap Model::SelectContours(float ratio_thr1, float ratio_thr2)
{
    cv::Mat mat = targetImage.clone();
    cv::Mat contourMat = mat.clone();
    cv::Mat grayMat;
    //convert to Gray
    cv::cvtColor(mat, grayMat, cv::COLOR_BGR2GRAY);
    cv::Mat object = cv::Mat::zeros(grayMat.rows, grayMat.cols, grayMat.type());
    cv::Rect compRect[255];
    int label_num = 100;

    int comp_idx = 0;
    cv::Mat temp;
    cv::threshold(grayMat, temp, 0, 255, cv::THRESH_BINARY|cv::THRESH_OTSU);
    object = temp.clone();

    for (int i = 0; i < object.rows; i++)
    {
        for (int j = 0; j < object.cols; j++)
        {
            cv::Scalar color = object.at<uchar>(cv::Point(j, i));
            if (color.val[0] == 255)
            {
                floodFill(object, cv::Point(j, i), cv::Scalar(label_num), &compRect[comp_idx]);
                label_num++; comp_idx++;
            }
        }
    }

    std::vector<std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours(temp, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_NONE);
    for (int i = comp_idx - 1; i >= 0; i--)
    {
        float parameter = arcLength(contours[i], true);
        float arcAngle = 4 * CV_PI * contourArea(contours[i]) / (parameter * parameter);

        if (arcAngle < ratio_thr1 && arcAngle > ratio_thr2)
        {
            cv::Scalar color = cv::Scalar(0, 0, 255);
            cv::drawContours(contourMat, contours, i, color, 2, 8, hierarchy);
        }
    }
    QPixmap des = MatConvertToQPixmap(contourMat);
    return des;
}

QPixmap Model::FindContours()
{
    cv::Mat mat = targetImage.clone();
    cv::Mat contourMat = mat.clone();
    cv::Mat grayMat;
    //convert to Gray
    cv::cvtColor(mat, grayMat, cv::COLOR_BGR2GRAY);
    cv::cvtColor(grayMat, grayMat, cv::COLOR_GRAY2RGB);

    //blur and canny
    cv::Mat edge;
    cv::blur(grayMat, grayMat, cv::Size(3,3));
    cv::Canny(grayMat, edge, 50, 150, 3);

    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::RNG rng(12345);

    cv::findContours(edge, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_NONE);
    for(int i=0; i<contours.size(); i++)
    {
        cv::Scalar color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), 255);
        cv::drawContours(contourMat, contours, i, color, 2, 8, hierarchy);
    }
    QPixmap des = MatConvertToQPixmap(contourMat);
    return des;

}

QPixmap Model::FindCircle()
{
    return SelectContours(1.0, 0.79);
}

QPixmap Model::FindTriangle()
{
    return SelectContours (0.6, 0.0);
}


QPixmap Model::FindSquare()
{
    return SelectContours(0.79, 0.6);
}

QPixmap Model::surf()
{
    int minHessian = 400;
    cv::Ptr<SURF> detector = SURF::create(minHessian);
    cv::Mat grayTarget, grayTemplate;
    // convert the image to gray scale
    cv::cvtColor(targetImage, grayTarget, cv::COLOR_BGR2GRAY);
    cv::cvtColor(templateImage, grayTemplate, cv::COLOR_BGR2GRAY);
    // detect the keypoints
    std::vector<cv::KeyPoint> keyPointTemplate, keyPointTarget;
    cv::Mat descriptorsTemplate, descriptorsTarget;

    detector->detectAndCompute( grayTemplate, cv::noArray(), keyPointTemplate, descriptorsTemplate );
    detector->detectAndCompute( grayTarget, cv::noArray(), keyPointTarget, descriptorsTarget );

    //-- Step 2: Matching descriptor vectors with a FLANN based matcher
    // Since SURF is a floating-point descriptor NORM_L2 is used
    cv::Ptr<cv::DescriptorMatcher> matcher = cv::DescriptorMatcher::create(cv::DescriptorMatcher::FLANNBASED);
    std::vector< std::vector<cv::DMatch> > knn_matches;
    matcher->knnMatch( descriptorsTemplate, descriptorsTarget, knn_matches, 2 );
    //-- Filter matches using the Lowe's ratio test
    const float ratio_thresh = 0.75f;
    std::vector<cv::DMatch> good_matches;
    for (size_t i = 0; i < knn_matches.size(); i++)
    {
        if (knn_matches[i][0].distance < ratio_thresh * knn_matches[i][1].distance)
        {
            good_matches.push_back(knn_matches[i][0]);
        }
    }

    //-- Localize the object
    std::vector<Point2f> obj;
    std::vector<Point2f> scene;
    for( size_t i = 0; i < good_matches.size(); i++ )
    {
        //-- Get the keypoints from the good matches
        obj.push_back( keyPointTemplate[ good_matches[i].queryIdx ].pt );
        scene.push_back( keyPointTarget[ good_matches[i].trainIdx ].pt );
    }

    //Draw matches
    cv::Mat imageMatches = targetImage.clone();
    //drawMatches(templateImage, keyPointTemplate, targetImage, keyPointTarget, good_matches, imageMatches, cv::Scalar::all(-1),
    //            cv::Scalar::all(-1), std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

    cv::Mat H = findHomography( obj, scene, RANSAC );

    //-- Get the corners from the image_1 ( the object to be "detected" )
    std::vector<Point2f> obj_corners(4);
    obj_corners[0] = Point2f(0, 0);
    obj_corners[1] = Point2f( (float)templateImage.cols, 0 );
    obj_corners[2] = Point2f( (float)templateImage.cols, (float)templateImage.rows );
    obj_corners[3] = Point2f( 0, (float)templateImage.rows );
    std::vector<cv::Point2f> scene_corners(4);
    perspectiveTransform( obj_corners, scene_corners, H);

    //-- Draw lines between the corners (the mapped object in the scene - image_2 )
    line( imageMatches, scene_corners[0],
          scene_corners[1], Scalar(0, 255, 0), 4 );
    line( imageMatches, scene_corners[1],
          scene_corners[2], Scalar( 0, 255, 0), 4 );
    line( imageMatches, scene_corners[2],
          scene_corners[3], Scalar( 0, 255, 0), 4 );
    line( imageMatches, scene_corners[3],
          scene_corners[0], Scalar( 0, 255, 0), 4 );

    std::cout << good_matches.size() << std::endl;

    //imshow("Good Matches & Object detection", imageMatches );
    //waitKey();

    return MatConvertToQPixmap(imageMatches);
}

