#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <opencv4/opencv2/opencv.hpp>
#include <QPixmap>
#include "Model.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_openTargetButton_clicked();

    void on_openTemplateButton_clicked();

    void on_findContoursButton_clicked();

    // find contours
    void on_circleButton_clicked();

    void on_triangleButton_clicked();

    void on_squareButton_clicked();

    void on_closeButton_clicked();

    void on_surfButton_clicked();

private:
    Ui::MainWindow *ui;
    Model model;
    QPixmap targetImage;
    QPixmap templateImage;
};
#endif // MAINWINDOW_H
