/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../opecvQtTest/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[25];
    char stringdata0[562];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 21), // "on_openButton_clicked"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 22), // "on_clearButton_clicked"
QT_MOC_LITERAL(4, 57, 22), // "on_closeButton_clicked"
QT_MOC_LITERAL(5, 80, 21), // "on_grayButton_clicked"
QT_MOC_LITERAL(6, 102, 23), // "on_eqHistButton_clicked"
QT_MOC_LITERAL(7, 126, 31), // "on_thresholdLineEdit_textEdited"
QT_MOC_LITERAL(8, 158, 4), // "arg1"
QT_MOC_LITERAL(9, 163, 26), // "on_thresholdButton_clicked"
QT_MOC_LITERAL(10, 190, 30), // "on_ThresholdOtsuButton_clicked"
QT_MOC_LITERAL(11, 221, 16), // "denoiseSetPixmap"
QT_MOC_LITERAL(12, 238, 14), // "morphSetPixmap"
QT_MOC_LITERAL(13, 253, 21), // "on_blurButton_clicked"
QT_MOC_LITERAL(14, 275, 27), // "on_medianBlurButton_clicked"
QT_MOC_LITERAL(15, 303, 29), // "on_gaussianBlurButton_clicked"
QT_MOC_LITERAL(16, 333, 30), // "on_bilateralBlurButton_clicked"
QT_MOC_LITERAL(17, 364, 23), // "on_dilateButton_clicked"
QT_MOC_LITERAL(18, 388, 22), // "on_erodeButton_clicked"
QT_MOC_LITERAL(19, 411, 26), // "on_morphOpenButton_clicked"
QT_MOC_LITERAL(20, 438, 27), // "on_morphCloseButton_clicked"
QT_MOC_LITERAL(21, 466, 25), // "on_gradientButton_clicked"
QT_MOC_LITERAL(22, 492, 23), // "on_topHatButton_clicked"
QT_MOC_LITERAL(23, 516, 25), // "on_blackHatButton_clicked"
QT_MOC_LITERAL(24, 542, 19) // "updateExecTimeLabel"

    },
    "MainWindow\0on_openButton_clicked\0\0"
    "on_clearButton_clicked\0on_closeButton_clicked\0"
    "on_grayButton_clicked\0on_eqHistButton_clicked\0"
    "on_thresholdLineEdit_textEdited\0arg1\0"
    "on_thresholdButton_clicked\0"
    "on_ThresholdOtsuButton_clicked\0"
    "denoiseSetPixmap\0morphSetPixmap\0"
    "on_blurButton_clicked\0on_medianBlurButton_clicked\0"
    "on_gaussianBlurButton_clicked\0"
    "on_bilateralBlurButton_clicked\0"
    "on_dilateButton_clicked\0on_erodeButton_clicked\0"
    "on_morphOpenButton_clicked\0"
    "on_morphCloseButton_clicked\0"
    "on_gradientButton_clicked\0"
    "on_topHatButton_clicked\0"
    "on_blackHatButton_clicked\0updateExecTimeLabel"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  124,    2, 0x08 /* Private */,
       3,    0,  125,    2, 0x08 /* Private */,
       4,    0,  126,    2, 0x08 /* Private */,
       5,    0,  127,    2, 0x08 /* Private */,
       6,    0,  128,    2, 0x08 /* Private */,
       7,    1,  129,    2, 0x08 /* Private */,
       9,    0,  132,    2, 0x08 /* Private */,
      10,    0,  133,    2, 0x08 /* Private */,
      11,    1,  134,    2, 0x08 /* Private */,
      12,    1,  137,    2, 0x08 /* Private */,
      13,    0,  140,    2, 0x08 /* Private */,
      14,    0,  141,    2, 0x08 /* Private */,
      15,    0,  142,    2, 0x08 /* Private */,
      16,    0,  143,    2, 0x08 /* Private */,
      17,    0,  144,    2, 0x08 /* Private */,
      18,    0,  145,    2, 0x08 /* Private */,
      19,    0,  146,    2, 0x08 /* Private */,
      20,    0,  147,    2, 0x08 /* Private */,
      21,    0,  148,    2, 0x08 /* Private */,
      22,    0,  149,    2, 0x08 /* Private */,
      23,    0,  150,    2, 0x08 /* Private */,
      24,    1,  151,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    8,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QPixmap,    2,
    QMetaType::Void, QMetaType::QPixmap,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Float,    2,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_openButton_clicked(); break;
        case 1: _t->on_clearButton_clicked(); break;
        case 2: _t->on_closeButton_clicked(); break;
        case 3: _t->on_grayButton_clicked(); break;
        case 4: _t->on_eqHistButton_clicked(); break;
        case 5: _t->on_thresholdLineEdit_textEdited((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->on_thresholdButton_clicked(); break;
        case 7: _t->on_ThresholdOtsuButton_clicked(); break;
        case 8: _t->denoiseSetPixmap((*reinterpret_cast< QPixmap(*)>(_a[1]))); break;
        case 9: _t->morphSetPixmap((*reinterpret_cast< QPixmap(*)>(_a[1]))); break;
        case 10: _t->on_blurButton_clicked(); break;
        case 11: _t->on_medianBlurButton_clicked(); break;
        case 12: _t->on_gaussianBlurButton_clicked(); break;
        case 13: _t->on_bilateralBlurButton_clicked(); break;
        case 14: _t->on_dilateButton_clicked(); break;
        case 15: _t->on_erodeButton_clicked(); break;
        case 16: _t->on_morphOpenButton_clicked(); break;
        case 17: _t->on_morphCloseButton_clicked(); break;
        case 18: _t->on_gradientButton_clicked(); break;
        case 19: _t->on_topHatButton_clicked(); break;
        case 20: _t->on_blackHatButton_clicked(); break;
        case 21: _t->updateExecTimeLabel((*reinterpret_cast< float(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 22)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 22;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
