/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QLabel *originalLabel;
    QLabel *grayLabel;
    QPushButton *openButton;
    QPushButton *clearButton;
    QLabel *denoiseLabel;
    QLabel *thresholdLabel;
    QLabel *morphologyLabel;
    QPushButton *closeButton;
    QPushButton *grayButton;
    QPushButton *eqHistButton;
    QPushButton *blurButton;
    QPushButton *thresholdButton;
    QPushButton *ThresholdOtsuButton;
    QPushButton *medianBlurButton;
    QPushButton *bilateralBlurButton;
    QPushButton *gaussianBlurButton;
    QLineEdit *thresholdLineEdit;
    QPushButton *dilateButton;
    QPushButton *morphCloseButton;
    QPushButton *erodeButton;
    QPushButton *morphOpenButton;
    QPushButton *gradientButton;
    QPushButton *blackHatButton;
    QPushButton *topHatButton;
    QLabel *label;
    QLabel *execTimeLabel;
    QLabel *label_2;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1018, 780);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        originalLabel = new QLabel(centralwidget);
        originalLabel->setObjectName(QString::fromUtf8("originalLabel"));
        originalLabel->setGeometry(QRect(30, 30, 240, 192));
        sizePolicy.setHeightForWidth(originalLabel->sizePolicy().hasHeightForWidth());
        originalLabel->setSizePolicy(sizePolicy);
        grayLabel = new QLabel(centralwidget);
        grayLabel->setObjectName(QString::fromUtf8("grayLabel"));
        grayLabel->setGeometry(QRect(30, 240, 240, 192));
        openButton = new QPushButton(centralwidget);
        openButton->setObjectName(QString::fromUtf8("openButton"));
        openButton->setGeometry(QRect(0, 450, 121, 31));
        clearButton = new QPushButton(centralwidget);
        clearButton->setObjectName(QString::fromUtf8("clearButton"));
        clearButton->setGeometry(QRect(0, 490, 121, 31));
        denoiseLabel = new QLabel(centralwidget);
        denoiseLabel->setObjectName(QString::fromUtf8("denoiseLabel"));
        denoiseLabel->setGeometry(QRect(340, 30, 240, 192));
        thresholdLabel = new QLabel(centralwidget);
        thresholdLabel->setObjectName(QString::fromUtf8("thresholdLabel"));
        thresholdLabel->setGeometry(QRect(340, 240, 240, 192));
        morphologyLabel = new QLabel(centralwidget);
        morphologyLabel->setObjectName(QString::fromUtf8("morphologyLabel"));
        morphologyLabel->setGeometry(QRect(670, 30, 240, 192));
        closeButton = new QPushButton(centralwidget);
        closeButton->setObjectName(QString::fromUtf8("closeButton"));
        closeButton->setGeometry(QRect(0, 530, 121, 31));
        grayButton = new QPushButton(centralwidget);
        grayButton->setObjectName(QString::fromUtf8("grayButton"));
        grayButton->setGeometry(QRect(130, 450, 101, 31));
        eqHistButton = new QPushButton(centralwidget);
        eqHistButton->setObjectName(QString::fromUtf8("eqHistButton"));
        eqHistButton->setGeometry(QRect(130, 490, 101, 31));
        blurButton = new QPushButton(centralwidget);
        blurButton->setObjectName(QString::fromUtf8("blurButton"));
        blurButton->setGeometry(QRect(280, 450, 101, 31));
        thresholdButton = new QPushButton(centralwidget);
        thresholdButton->setObjectName(QString::fromUtf8("thresholdButton"));
        thresholdButton->setGeometry(QRect(390, 450, 111, 31));
        ThresholdOtsuButton = new QPushButton(centralwidget);
        ThresholdOtsuButton->setObjectName(QString::fromUtf8("ThresholdOtsuButton"));
        ThresholdOtsuButton->setGeometry(QRect(390, 490, 111, 31));
        medianBlurButton = new QPushButton(centralwidget);
        medianBlurButton->setObjectName(QString::fromUtf8("medianBlurButton"));
        medianBlurButton->setGeometry(QRect(280, 490, 101, 31));
        bilateralBlurButton = new QPushButton(centralwidget);
        bilateralBlurButton->setObjectName(QString::fromUtf8("bilateralBlurButton"));
        bilateralBlurButton->setGeometry(QRect(280, 570, 101, 31));
        gaussianBlurButton = new QPushButton(centralwidget);
        gaussianBlurButton->setObjectName(QString::fromUtf8("gaussianBlurButton"));
        gaussianBlurButton->setGeometry(QRect(280, 530, 101, 31));
        thresholdLineEdit = new QLineEdit(centralwidget);
        thresholdLineEdit->setObjectName(QString::fromUtf8("thresholdLineEdit"));
        thresholdLineEdit->setGeometry(QRect(510, 450, 41, 27));
        thresholdLineEdit->setLayoutDirection(Qt::LeftToRight);
        thresholdLineEdit->setMaxLength(3);
        thresholdLineEdit->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        dilateButton = new QPushButton(centralwidget);
        dilateButton->setObjectName(QString::fromUtf8("dilateButton"));
        dilateButton->setGeometry(QRect(600, 450, 101, 31));
        morphCloseButton = new QPushButton(centralwidget);
        morphCloseButton->setObjectName(QString::fromUtf8("morphCloseButton"));
        morphCloseButton->setGeometry(QRect(600, 570, 101, 31));
        erodeButton = new QPushButton(centralwidget);
        erodeButton->setObjectName(QString::fromUtf8("erodeButton"));
        erodeButton->setGeometry(QRect(600, 490, 101, 31));
        morphOpenButton = new QPushButton(centralwidget);
        morphOpenButton->setObjectName(QString::fromUtf8("morphOpenButton"));
        morphOpenButton->setGeometry(QRect(600, 530, 101, 31));
        gradientButton = new QPushButton(centralwidget);
        gradientButton->setObjectName(QString::fromUtf8("gradientButton"));
        gradientButton->setGeometry(QRect(710, 450, 101, 31));
        blackHatButton = new QPushButton(centralwidget);
        blackHatButton->setObjectName(QString::fromUtf8("blackHatButton"));
        blackHatButton->setGeometry(QRect(710, 530, 101, 31));
        topHatButton = new QPushButton(centralwidget);
        topHatButton->setObjectName(QString::fromUtf8("topHatButton"));
        topHatButton->setGeometry(QRect(710, 490, 101, 31));
        label = new QLabel(centralwidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(520, 610, 81, 20));
        execTimeLabel = new QLabel(centralwidget);
        execTimeLabel->setObjectName(QString::fromUtf8("execTimeLabel"));
        execTimeLabel->setGeometry(QRect(600, 610, 61, 19));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(660, 610, 65, 19));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1018, 24));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        originalLabel->setText(QCoreApplication::translate("MainWindow", "Original Image", nullptr));
        grayLabel->setText(QCoreApplication::translate("MainWindow", "Gray Image", nullptr));
        openButton->setText(QCoreApplication::translate("MainWindow", "Open Image", nullptr));
        clearButton->setText(QCoreApplication::translate("MainWindow", "Clear Processing", nullptr));
        denoiseLabel->setText(QCoreApplication::translate("MainWindow", "Denoise Image", nullptr));
        thresholdLabel->setText(QCoreApplication::translate("MainWindow", "Threshold Image", nullptr));
        morphologyLabel->setText(QCoreApplication::translate("MainWindow", "Morphology Image", nullptr));
        closeButton->setText(QCoreApplication::translate("MainWindow", "Close App", nullptr));
        grayButton->setText(QCoreApplication::translate("MainWindow", "Gray", nullptr));
        eqHistButton->setText(QCoreApplication::translate("MainWindow", "EqHist", nullptr));
        blurButton->setText(QCoreApplication::translate("MainWindow", "Blur", nullptr));
        thresholdButton->setText(QCoreApplication::translate("MainWindow", "Threshold", nullptr));
        ThresholdOtsuButton->setText(QCoreApplication::translate("MainWindow", "Threshold (otsu)", nullptr));
        medianBlurButton->setText(QCoreApplication::translate("MainWindow", "Median Blur", nullptr));
        bilateralBlurButton->setText(QCoreApplication::translate("MainWindow", "Bilateral Blur", nullptr));
        gaussianBlurButton->setText(QCoreApplication::translate("MainWindow", "Gaussian Blur", nullptr));
        thresholdLineEdit->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        dilateButton->setText(QCoreApplication::translate("MainWindow", "Dilate", nullptr));
        morphCloseButton->setText(QCoreApplication::translate("MainWindow", "Close", nullptr));
        erodeButton->setText(QCoreApplication::translate("MainWindow", "Erode", nullptr));
        morphOpenButton->setText(QCoreApplication::translate("MainWindow", "Open", nullptr));
        gradientButton->setText(QCoreApplication::translate("MainWindow", "Gradient", nullptr));
        blackHatButton->setText(QCoreApplication::translate("MainWindow", "BlackHat", nullptr));
        topHatButton->setText(QCoreApplication::translate("MainWindow", "TopHat", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "Time Cost:", nullptr));
        execTimeLabel->setText(QString());
        label_2->setText(QCoreApplication::translate("MainWindow", "(ms)", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
