#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QPixmap>
#include <QImage>
#include <iostream>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow), model()
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_openButton_clicked()
{
    time = clock();
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Image"), "/home", tr("Images (*.png *.jpg *.bmp)"));
    QPixmap image(fileName);
    model.setOriginalImage(image);
    image = image.scaled(ui->originalLabel->size(), Qt::KeepAspectRatio);
    ui->originalLabel->setPixmap(image);
    updateExecTimeLabel(float(clock() - time)/CLOCKS_PER_SEC);
}

void MainWindow::on_clearButton_clicked()
{
    time = clock();
    ui->grayLabel->clear();
    ui->denoiseLabel->clear();
    ui->thresholdLabel->clear();
    ui->morphologyLabel->clear();
    updateExecTimeLabel(float(clock() - time)/CLOCKS_PER_SEC);
}

void MainWindow::on_closeButton_clicked()
{
    time = clock();
    this->close();
    updateExecTimeLabel(float(clock() - time)/CLOCKS_PER_SEC);
}

void MainWindow::on_grayButton_clicked()
{
    time = clock();
    QPixmap img = model.convertToGray();
    img = img.scaled(ui->grayLabel->size(), Qt::KeepAspectRatio);
    ui->grayLabel->setPixmap(img);
    updateExecTimeLabel(float(clock() - time)/CLOCKS_PER_SEC);
}

void MainWindow::on_eqHistButton_clicked()
{
    time = clock();
    QPixmap img = model.convertToEqHist();
    img = img.scaled(ui->grayLabel->size(), Qt::KeepAspectRatio);
    ui->grayLabel->setPixmap(img);
    updateExecTimeLabel(float(clock() - time)/CLOCKS_PER_SEC);
}

void MainWindow::on_thresholdLineEdit_textEdited(const QString &arg1)
{
    bool convertOk;
    unsigned int thres = ui->thresholdLineEdit->text().toUInt(&convertOk);
    if (!convertOk || thres > 255) {   // 0 <= thres <= 255
        ui->thresholdLineEdit->setText("0");
    }
    else {
        model.setThreshold(thres);
        on_thresholdButton_clicked();
    }
}

void MainWindow::on_thresholdButton_clicked()
{
    time = clock();
    QPixmap img = model.convertToThresBin();
    img = img.scaled(ui->thresholdLabel->size(), Qt::KeepAspectRatio);
    ui->thresholdLabel->setPixmap(img);
    updateExecTimeLabel(float(clock() - time)/CLOCKS_PER_SEC);
}

void MainWindow::on_ThresholdOtsuButton_clicked()
{
    time = clock();
    QPixmap img = model.convertToThresOtsu();
    img = img.scaled(ui->thresholdLabel->size(), Qt::KeepAspectRatio);
    ui->thresholdLabel->setPixmap(img);
    updateExecTimeLabel(float(clock() - time)/CLOCKS_PER_SEC);
}

void MainWindow::denoiseSetPixmap (QPixmap img)
{
    time = clock();
    img = img.scaled(ui->denoiseLabel->size(), Qt::KeepAspectRatio);
    ui->denoiseLabel->setPixmap(img);
    updateExecTimeLabel(float(clock() - time)/CLOCKS_PER_SEC);
}

void MainWindow::morphSetPixmap (QPixmap img) {
    img = img.scaled(ui->morphologyLabel->size(), Qt::KeepAspectRatio);
    ui->morphologyLabel->setPixmap(img);
    updateExecTimeLabel(float(clock() - time)/CLOCKS_PER_SEC);
}

void MainWindow::on_blurButton_clicked()
{
    time = clock();
    QPixmap img = model.convertToDenoise("blur");
    denoiseSetPixmap(img);
    updateExecTimeLabel(float(clock() - time)/CLOCKS_PER_SEC);
}

void MainWindow::on_medianBlurButton_clicked()
{
    time = clock();
    QPixmap img = model.convertToDenoise("median");
    denoiseSetPixmap(img);
    updateExecTimeLabel(float(clock() - time)/CLOCKS_PER_SEC);
}

void MainWindow::on_gaussianBlurButton_clicked()
{
    time = clock();
    QPixmap img = model.convertToDenoise("gaussian");
    denoiseSetPixmap(img);
    updateExecTimeLabel(float(clock() - time)/CLOCKS_PER_SEC);
}

void MainWindow::on_bilateralBlurButton_clicked()
{
    time = clock();
    QPixmap img = model.convertToDenoise("bilateral");
    denoiseSetPixmap(img);
    updateExecTimeLabel(float(clock() - time)/CLOCKS_PER_SEC);
}

void MainWindow::on_dilateButton_clicked()
{
    time = clock();
    QPixmap img = model.convertToDilate(ui->thresholdLabel->pixmap(), "d");
    morphSetPixmap(img);
    updateExecTimeLabel(float(clock() - time)/CLOCKS_PER_SEC);
}


void MainWindow::on_erodeButton_clicked()
{
    time = clock();
    QPixmap img = model.convertToDilate(ui->thresholdLabel->pixmap(), "e");
    morphSetPixmap(img);
    updateExecTimeLabel(float(clock() - time)/CLOCKS_PER_SEC);
}

void MainWindow::on_morphOpenButton_clicked()
{
    time = clock();
    QPixmap img = model.convertToMorph(ui->thresholdLabel->pixmap(), cv::MORPH_OPEN);
    morphSetPixmap(img);
    updateExecTimeLabel(float(clock() - time)/CLOCKS_PER_SEC);
}

void MainWindow::on_morphCloseButton_clicked()
{
    time = clock();
    QPixmap img = model.convertToMorph(ui->thresholdLabel->pixmap(), cv::MORPH_CLOSE);
    morphSetPixmap(img);
    updateExecTimeLabel(float(clock() - time)/CLOCKS_PER_SEC);
}

void MainWindow::on_gradientButton_clicked()
{
    time = clock();
    QPixmap img = model.convertToMorph(ui->thresholdLabel->pixmap(), cv::MORPH_GRADIENT);
    morphSetPixmap(img);
    updateExecTimeLabel(float(clock() - time)/CLOCKS_PER_SEC);
}

void MainWindow::on_topHatButton_clicked()
{
    time = clock();
    QPixmap img = model.convertToMorph(ui->thresholdLabel->pixmap(), cv::MORPH_TOPHAT);
    morphSetPixmap(img);
    updateExecTimeLabel(float(clock() - time)/CLOCKS_PER_SEC);
}

void MainWindow::on_blackHatButton_clicked()
{
    time = clock();
    QPixmap img = model.convertToMorph(ui->thresholdLabel->pixmap(), cv::MORPH_BLACKHAT);
    morphSetPixmap(img);
    updateExecTimeLabel(float(clock() - time)/CLOCKS_PER_SEC);
}

void MainWindow::updateExecTimeLabel(float timeInSeconds)
{
    int timeInMs = (int)(timeInSeconds * 1000);
    ui->execTimeLabel->setText(QString(std::to_string(timeInMs).c_str()));
}
