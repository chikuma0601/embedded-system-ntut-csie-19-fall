#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <ctime>

#include "model.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_openButton_clicked();

    void on_clearButton_clicked();

    void on_closeButton_clicked();

    void on_grayButton_clicked();

    void on_eqHistButton_clicked();

    void on_thresholdLineEdit_textEdited(const QString &arg1);

    void on_thresholdButton_clicked();

    void on_ThresholdOtsuButton_clicked();

    void denoiseSetPixmap(QPixmap);

    void morphSetPixmap (QPixmap);

    void on_blurButton_clicked();

    void on_medianBlurButton_clicked();

    void on_gaussianBlurButton_clicked();

    void on_bilateralBlurButton_clicked();

    void on_dilateButton_clicked();

    void on_erodeButton_clicked();

    void on_morphOpenButton_clicked();

    void on_morphCloseButton_clicked();

    void on_gradientButton_clicked();

    void on_topHatButton_clicked();

    void on_blackHatButton_clicked();

    void updateExecTimeLabel(float);

private:
    Ui::MainWindow *ui;
    Model model;
    clock_t time;
};
#endif // MAINWINDOW_H
