#ifndef MODEL_H
#define MODEL_H

#include <QPixmap>
#include <QImage>
#include <opencv2/opencv.hpp>


class Model
{
public:
    Model();
    ~Model();
    void setOriginalImage (const QPixmap &);
    void setThreshold(unsigned int);
    cv::Mat * convertPixmapToMat (const QPixmap &);
    cv::Mat srcImageToGray();
    QPixmap convertMatToPixmap (const cv::Mat &);
    QPixmap convertToGray ();
    QPixmap convertToEqHist ();
    QPixmap convertToThresBin();
    QPixmap convertToThresOtsu();
    QPixmap convertToDenoise(std::string);
    QPixmap convertToDilate(const QPixmap*, std::string);
    QPixmap convertToMorph(const QPixmap*, cv::MorphTypes);

    // debug function
    void showImage (cv::Mat, std::string);
private:
    cv::Mat * originalImage = nullptr;
    unsigned int threshold = 0;
};

#endif // MODEL_H
