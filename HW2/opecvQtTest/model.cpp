#include "model.h"

Model::Model () {}

Model::~Model () {
    delete originalImage;
}

void Model::setOriginalImage (const QPixmap & originalImage) {
    delete this->originalImage;
    this->originalImage = convertPixmapToMat(originalImage);
}

void Model::setThreshold(unsigned int threshold) {
    this->threshold = threshold;
}

cv::Mat * Model::convertPixmapToMat(const QPixmap &pixmap)
{
    QImage image = pixmap.toImage();
    cv::Mat * mat = new cv::Mat(image.height(), image.width(), CV_8UC4,
                                const_cast<uchar*>(image.bits()),
                                static_cast<size_t>(image.bytesPerLine()));
    cv::cvtColor(*mat, *mat, cv::COLOR_BGRA2BGR);
    return mat;
}

cv::Mat Model::srcImageToGray () {
    cv::Mat grayMat;
    cv::cvtColor(*originalImage, grayMat, cv::COLOR_BGR2GRAY);
    return grayMat;
}

QPixmap Model::convertMatToPixmap(const cv::Mat &inMat)
{
    QImage image(inMat.data, inMat.cols, inMat.rows,
                 static_cast<int>(inMat.step),
                 QImage::Format_RGB888);    // Actually irrevalent, convertFromImage will automantically distinguish gray and color
    QPixmap pixmap;
    pixmap.convertFromImage(image);
    return pixmap;
}

QPixmap Model::convertToGray () {
    cv::Mat grayMat = srcImageToGray();
    cv::cvtColor(grayMat, grayMat, cv::COLOR_GRAY2RGB);

    return convertMatToPixmap(grayMat);
}

QPixmap Model::convertToEqHist () {
    cv::Mat grayMat = srcImageToGray();
    cv::equalizeHist(grayMat, grayMat);
    cv::cvtColor(grayMat, grayMat, cv::COLOR_GRAY2RGB);

    return convertMatToPixmap(grayMat);
}

QPixmap Model::convertToThresBin () {
    cv::Mat thresBin = srcImageToGray();
    cv::threshold(thresBin, thresBin, threshold, 255, cv::THRESH_BINARY);
    cv::cvtColor(thresBin, thresBin, cv::COLOR_GRAY2RGB);

    return convertMatToPixmap(thresBin);
}

QPixmap Model::convertToThresOtsu () {
    cv::Mat thresBin = srcImageToGray();
    cv::threshold(thresBin, thresBin, threshold, 255, cv::THRESH_OTSU);
    cv::cvtColor(thresBin, thresBin, cv::COLOR_GRAY2RGB);

    return convertMatToPixmap(thresBin);
}

QPixmap Model::convertToDenoise(std::string type) {
    cv::Mat origin = srcImageToGray();
    cv::Mat denoise;
    if (type == "blur") {cv::blur(origin, denoise, cv::Size(3, 3));}
    else if (type == "median") {cv::medianBlur(origin, denoise, 3);}
    else if (type == "gaussian") {cv::GaussianBlur(origin, denoise, cv::Size(3, 3), 0.02, 0.02);}
    else if (type == "bilateral") {cv::bilateralFilter(origin, denoise, 10, 20, 5);}
    cv::cvtColor(denoise, denoise, cv::COLOR_GRAY2RGB);

    return convertMatToPixmap(denoise);
}

QPixmap Model::convertToDilate(const QPixmap * pmap, std::string type) {
    cv::Mat * dilateMt = convertPixmapToMat(*pmap);
    cv::cvtColor(*dilateMt, *dilateMt, cv::COLOR_RGB2GRAY);

    if (type == "d")
        cv::dilate(*dilateMt, *dilateMt, cv::Mat());
    else if (type == "e")
        cv::erode(*dilateMt, *dilateMt, cv::Mat());

    cv::cvtColor(*dilateMt, *dilateMt, cv::COLOR_GRAY2RGB);
    cv::Mat cykaBlyat(*dilateMt);
    delete dilateMt;
    return convertMatToPixmap(cykaBlyat);
}

QPixmap Model::convertToMorph(const QPixmap * pmap, cv::MorphTypes type) {
    cv::Mat * morph = convertPixmapToMat(*pmap);
    cv::cvtColor(*morph, *morph, cv::COLOR_RGB2GRAY);

    cv::morphologyEx(*morph, *morph, type, cv::Mat(), cv::Point(-1,-1), 2);

    cv::cvtColor(*morph, *morph, cv::COLOR_GRAY2RGB);
    cv::Mat cykaBlyat(*morph);
    delete morph;
    return convertMatToPixmap(cykaBlyat);
}


void Model::showImage (cv::Mat img, std::string title) {
    cv::imshow(title, img.clone());
    cv::waitKey(0);
}
